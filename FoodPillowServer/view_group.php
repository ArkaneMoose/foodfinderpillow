<?php 
session_start();
include('config.php'); 
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
?>
<?php include('header.php'); ?>
<?php
$groupid = $db->escape_string($_GET['groupid']);
$query = "SELECT groups.groupname, users.userid, users.username FROM groups LEFT JOIN usergroups ON groups.groupid=usergroups.groupid LEFT JOIN users ON usergroups.userid=users.userid WHERE groups.groupid=$groupid;";
if (!$query_result = $db->query($query)) {
    ?>
    <p>Failed to connect to database. Try <a href="group_current.php">reloading</a> the page.</p>
    <p><strong>Error details:</strong><br><?php
        echo(htmlspecialchars($db->error));
    ?></p>
    <?php
}
if ($query_result->num_rows == 0) {
    ?>
    <p>The specified group does not exist.</p>
    <?php
} else {
    $group_row = $query_result->fetch_assoc();
    $group_name = $group_row['groupname'];
    $group_users = array();
    $viewer_is_in_group = false;
    do {
        if ($group_row['userid'] == $_SESSION['userid']) {
            $group_users[count($group_users)] = 'You';
            $viewer_is_in_group = true;
        } else if (isset($group_row['username'])) {
            $group_users[count($group_users)] = $group_row['username'];
        }
    } while ($group_row = $query_result->fetch_assoc());
    ?>
    <div class="page-header">
        <h3><?php echo(htmlspecialchars($group_name)) ?></h3>
        <?php
        if ($viewer_is_in_group) {
        ?>
        <form role="form" action="group_rename.php" method="GET">
            <input type="hidden" name="groupid" id="groupid" value="<?php echo(htmlspecialchars($_GET['groupid'])) ?>">
            <button type="submit" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-pencil"></span> Edit group name</button>
        </form>
        <?php
        }
        ?>
    </div>
    <?php
    if ($viewer_is_in_group) {
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Referral Code</h2>
        </div>
        <div class="panel-body">
            <p><input readonly value="<?php echo(htmlspecialchars($_GET['groupid'])) ?>" class="form-control input-lg" onclick="this.select();"></p>
            <p>Share this code with your friends so that they can add themselves to this group.</p>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">People in this group</h2>
        </div>
        <div class="panel-body">
            <p><?php
            if (count($group_users) == 0) echo('There are no people in this group.');
            else {
                foreach ($group_users as $index => $user) {
                    if (count($group_users) > 2 && $index > 0) echo(', ');
                    else if (count($group_users) > 1 && $index > 0) echo(' ');
                    if (count($group_users) > 1 && $index == count($group_users) - 1) echo('and ');
                    echo(htmlspecialchars($user));
                }
            }
            ?></p>
            <?php
            if (!$viewer_is_in_group) {
                ?>
                <form role="form" action="group_join_process.php" method="POST">
                    <input type="hidden" name="groupid" id="groupid" value="<?php echo(htmlspecialchars($_GET['groupid'])); ?>">
                    <button type="submit" class="btn btn-primary">Join this group</button>
                </form>
                <?php
            } else {
                ?>
                <form role="form" action="group_leave_process.php" method="POST">
                    <input type="hidden" name="groupid" id="groupid" value="<?php echo(htmlspecialchars($_GET['groupid'])); ?>">
                    <button type="submit" class="btn btn-danger">Leave this group</button>
                </form>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Get group suggestions</h2>
        </div>
        <div class="panel-body">
            <p><a href="group_decide.php?groupid=<?php echo(htmlspecialchars($_GET['groupid'])); ?>" class="btn btn-default" role="button">Get group suggestions</a></p>
        </div>
    </div>
<?php
}
?>
<?php include('footer.php'); ?>