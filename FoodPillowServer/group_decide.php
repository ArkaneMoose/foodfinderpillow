<?php
session_start();
include('config.php');
require_once('../lib/yelp/yelp_api.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
if (!isset($_GET['groupid'])) {
    header('Location: group_current.php?Error=Missing+params');
}
?>
<?php include('header.php'); ?>
<div class="page-header">
        <h3>Group Suggestions</h3>
    </div>
<?php
$groupid = $db->escape_string($_GET['groupid']);
$groupuser_query = "SELECT userid from usergroups WHERE groupid='$groupid'";

?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Match</h2>
        </div>
        <div class="panel-body">
            
            <?php
            $yelp_data = yelp_search('', isset($_GET['location'])?$_GET['location']: '', null);
                $yelp_json = json_decode($yelp_data, TRUE);
                if (!isset($yelp_json['error'])) {
                    $yelp_business = $yelp_json["businesses"];
                    foreach($yelp_business as $key=>$val) {
                        if ($key > 4) break;
                        $business_name = $val['name'];
                        echo("<tr>");
                        if ($key == 0) {
                            echo("<td style=\"border-top-width: 0px;\">");
                        } else {
                            echo("<td>");
                        }
                        echo("<div class=\"row\">");
                        echo("<div class=\"col-md-4\">");
                        echo($business_name);
                        echo("</div>");
                        echo("<div class=\"col-md-8\">");
                        echo("<div class=\"btn-group\" id=\"buttons_" . $key . "\">");
                        for ($i = 0; $i < 4; $i++) {
                            $rating = $i + 1;
                            $business_types = $val['categories'];
                            $business_category = "";
                            for ($j = 0; $j < sizeof($business_types); $j++) {
                                $business_category .= (($j != 0) ? "," : "") . $business_types[$j][1];
                            }
                            trim($business_category, ",");
                            $business_name = str_replace("'", "", $business_name);
                            $business_name = str_replace(" ", "+", $business_name);
                            $request_string = "info_restaurant.php?restaurantname=" . $business_name . "&restauranttype=" . $business_category . "&rating=" . $rating;
                            $button_id = "rate_button_" . $key . "_" . $i;
                            //echo("<button type='button' class='btn btn-default btn-lg' onClick='buttonUpdate(" . $i . ", \"" . $button_id . "\",\"" . $request_string . "\")' id='" . $button_id . "'><span class='glyphicon glyphicon-heart-empty' aria-hidden='true'></span></button>");
                        }
                        echo("</div>");
                        $request_string = "info_restaurant.php?restaurantname=" . $business_name . "&restauranttype=" . $business_category . "&rating=0";
                        //echo("<button type='button' class='btn btn-default btn-lg' onClick='cancelButtonUpdate(\"buttons_" . $key . "\",\"" . $button_id . "\",\"" . $request_string . "\")' id='" . $button_id . "'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>");
                        echo("</div>");
                        echo("</td>");
                        echo("</div>");
                        echo("</tr>");
                    }
                }
                    ?>
        </div>
    </div>
<?php
//}
?>
<?php include('footer.php'); ?>