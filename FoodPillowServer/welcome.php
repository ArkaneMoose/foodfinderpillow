<?php
session_start();
include('config.php');
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
$userid = $db->escape_string($_SESSION['userid']); // is only set by server anyways
$train_check_query = "SELECT prefid FROM userpreferences WHERE userid='$userid'";
if (!$train_check_result = $db->query($train_check_query)) {
    die("unable to check pref train because " . $db->error);
}
$MIN_TRAIN = 10;
$need_train = $train_check_result->num_rows < $MIN_TRAIN;
if ($need_train) {
    header('Location: user_train.php');
} else {
    header('Location: group_menu.php');
}
?>