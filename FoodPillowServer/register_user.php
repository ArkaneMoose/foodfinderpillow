<?php 
session_start();
include('config.php'); 
?>
<?php
if (isset($_SESSION['userid'])) {
    header('Location: welcome.php');
}
?>
<?php include('header.php'); ?>
<div class ="page-header">
        <h3>Register</h3>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Register</h2>
        </div>
        <div class="panel-body">
            <form action="register_process.php" method="post">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="email-addon">@</span>
                    <input type="text" name="username" class="form-control" placeholder="Username" aria-describedby="email-addon">
                </div>
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="password-addon">*</span>
                    <input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="password-addon">
                </div>
                <br>
                <input type="submit" value="Register" class="btn btn-primary">
            </form>
        </div>
    </div>
<?php include('footer.php'); ?>