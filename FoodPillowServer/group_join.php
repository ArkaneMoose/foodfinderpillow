<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
?>
<?php include('header.php'); ?>
<div class="page-header">
        <h3>Join</h3>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Join a Group</h2>
        </div>
        <div class="panel-body">
            <form action="group_join_process.php" method="post">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="group-addon">#</span>
                    <input type="number" name="groupid" class="form-control" placeholder="Group ID" aria-describedby="group-addon">
                </div>
                <br>
                <input type="submit" value="Join" class="btn btn-primary">
            </form>
        </div>
    </div>
<?php include('footer.php'); ?>