<?php
session_start();
if (isset($_SESSION['userid'])) {
    header('Location: welcome.php');
} else {
    header('Location: login_user.php');
}
?>