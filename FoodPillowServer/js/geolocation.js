(function () {
    if (window.location.search.match(/(^\?|&)location=/i)) return;
    if (navigator.geolocation) navigator.geolocation.getCurrentPosition(function (position) {
        window.location.href = (window.location.search) ? window.location.href + "&location=" + position.coords.latitude + "," + position.coords.longitude : window.location.href + "?location=" + position.coords.latitude + "%2C" + position.coords.longitude;
    });
})();