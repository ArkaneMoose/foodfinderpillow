(function () {
    var container = document.querySelector("body > .container");
    var queryParams = window.location.search.substring(1).split("&");
    var decodeParameter = function (string) {
        return decodeURIComponent(string.replace(/\+/g, " "));
    };
    for (var index in queryParams) {
        var queryParam = queryParams[index];
        var equalsPosition = queryParam.indexOf("=");
        var key = decodeParameter(queryParam.substring(0, equalsPosition));
        var value = decodeParameter(queryParam.substring(equalsPosition + 1));
        var alertStyle;
        switch (key.toLowerCase()) {
            case "error":
            case "alert":
            case "danger":
            case "red":
                alertStyle = "danger";
                break;
            case "warn":
            case "warning":
            case "yellow":
                alertStyle = "warning";
                break;
            case "info":
            case "information":
            case "blue":
                alertStyle = "info";
                break;
            case "success":
            case "successful":
            case "green":
                alertStyle = "success";
                break;
        }
        if (!alertStyle) continue;
        var alert = document.createElement("div");
        alert.className = "fade in alert alert-" + alertStyle;
        var closeButton = document.createElement("a");
        closeButton.href = "#";
        closeButton.className = "close";
        closeButton.setAttribute("data-dismiss", "alert");
        closeButton.setAttribute("aria-label", "close");
        closeButton.appendChild(document.createTextNode("\u00D7"));
        alert.appendChild(closeButton);
        var regexMatched = value.match(/^(\w+(?!\w)\S)(\s+[\s\S]*)$/);
        if (regexMatched) {
            var strongText = document.createElement("strong");
            strongText.appendChild(document.createTextNode(regexMatched[1]));
            alert.appendChild(strongText);
            alert.appendChild(document.createTextNode(regexMatched[2]));
        } else {
            alert.appendChild(document.createTextNode(value));
        }
        container.insertBefore(alert, container.firstChild);
    }
})();