<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
?>
<?php include('header.php'); ?>
<div class="page-header">
        <h3>Menu</h3>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Group Menu</h2>
        </div>
        <div class="panel-body">
            <table class="table">
                <tr><td style="text-align: center; border-top-width: 0px;"><a style="min-width: 250px" class="btn btn-primary btn-lg" href="group_current.php">Current Groups</a></td></tr>
                <tr><td style="text-align: center;"><a style="min-width: 250px" class="btn btn-default btn-success btn-lg" href="group_join.php">Join a Group</a></td></tr>
                <tr><td style="text-align: center;"><a style="min-width: 250px" class="btn btn-default btn-danger btn-lg" href="group_create.php">Create a Group</a></td></tr>
            </table>
        </div>
    </div>
<?php include('footer.php'); ?>