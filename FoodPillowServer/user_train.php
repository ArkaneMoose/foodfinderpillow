<?php 
session_start();
include('config.php'); 
require_once('../lib/yelp/yelp_api.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
?>
<?php include('header.php'); ?>
<script type="text/javascript">
    function getAsync(url) {
        $("#loadWindow").load(url);
    }
    function buttonUpdate(index, btn_id, url) {
        var btnObject = document.getElementById(btn_id);
        for (var i = 0; i < 4; i++) {
            if (i <= index) {
                btnObject.parentNode.childNodes[i].innerHTML = "<span class='glyphicon glyphicon-heart' aria-hidden='true'></span>";
            } else {
                btnObject.parentNode.childNodes[i].innerHTML = "<span class='glyphicon glyphicon-heart-empty' aria-hidden='true'></span>";
            }
        }
        btnObject.innerHTML = "<span class='glyphicon glyphicon-heart' aria-hidden='true'></span>";
        getAsync(url);
    }
    function cancelButtonUpdate(btns_id, btn_id, url) {
        var btnObject = document.getElementById(btns_id);
        for (var i = 0; i < 4; i++) {
            btnObject.childNodes[i].innerHTML = "<span class='glyphicon glyphicon-heart-empty' aria-hidden='true'></span>";
        }
        getAsync(url);
    }
</script>
<div class="page-header">
        <h3>Preferences</h3>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Dietary restrictions</h2>
        </div>
        <div class="panel-body">
            <p>Check the boxes below if you must eat foods that are...</p>
            <form role="form" action="" method="GET">
                <?php
                foreach ($_GET as $key => $value) {
                    if (strtolower($key) == 'glutenfree' || strtolower($key) == 'vegetarian' || strtolower($key) == 'vegan') continue;
                    echo('<input type="hidden" name="' . htmlspecialchars($key) . '" value="' . htmlspecialchars($value) . '">');
                }
                ?>
                <div class="checkbox">
                    <label><input type="checkbox" name="glutenfree" id="glutenfree"<?php echo(isset($_GET["glutenfree"]) ? ' checked="checked"' : '') ?>> Gluten-free</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="vegetarian" id="vegetarian"<?php echo(isset($_GET["vegetarian"]) ? ' checked="checked"' : '') ?>> Vegetarian</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="vegan" id="vegan"<?php echo(isset($_GET["vegan"]) ? ' checked="checked"' : '') ?>> Vegan</label>
                </div>
                <button type="submit" class="btn btn-primary">Filter restaurants</button>
            </form>
            <p>Your restaurant suggestions will be limited to restaurants with options that accommodate your dietary restrictions.</p>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="loadWindow" style="display: none;">
            
        </div>
        <div class="panel-heading">
            <h2 class="panel-title">Preferences Training</h2>
        </div>
        <div class="panel-body">
            <table class="table">
                <?php
                $categories = isset($_GET['categories'])?$_GET['categories']: '';
                if (isset($_GET['glutenfree'])) $categories .= ',gluten_free';
                if (isset($_GET['vegetarian'])) $categories .= ',vegetarian';
                if (isset($_GET['vegan'])) $categories .= ',vegan';
                if (substr($categories, 0, 1) == ',') $categories = substr($categories, 1);
                $yelp_data = yelp_search($categories, isset($_GET['location'])?$_GET['location']: '', null);
                $yelp_json = json_decode($yelp_data, TRUE);
                if (!isset($yelp_json['error'])) {
                    $yelp_business = $yelp_json["businesses"];
                    foreach($yelp_business as $key=>$val) {
                        $business_name = $val['name'];
                        echo("<tr>");
                        if ($key == 0) {
                            echo("<td style=\"border-top-width: 0px;\">");
                        } else {
                            echo("<td>");
                        }
                        echo("<div class=\"row\">");
                        echo("<div class=\"col-md-4\">");
                        echo($business_name);
                        echo("</div>");
                        echo("<div class=\"col-md-8\">");
                        echo("<div class=\"btn-group\" id=\"buttons_" . $key . "\">");
                        for ($i = 0; $i < 4; $i++) {
                            $rating = $i + 1;
                            $business_types = $val['categories'];
                            $business_category = "";
                            for ($j = 0; $j < sizeof($business_types); $j++) {
                                $business_category .= (($j != 0) ? "," : "") . $business_types[$j][1];
                            }
                            trim($business_category, ",");
                            $business_name = str_replace("'", "", $business_name);
                            $business_name = str_replace(" ", "+", $business_name);
                            $request_string = "info_restaurant.php?restaurantname=" . $business_name . "&restauranttype=" . $business_category . "&rating=" . $rating;
                            $button_id = "rate_button_" . $key . "_" . $i;
                            echo("<button type='button' class='btn btn-default btn-lg' onClick='buttonUpdate(" . $i . ", \"" . $button_id . "\",\"" . $request_string . "\")' id='" . $button_id . "'><span class='glyphicon glyphicon-heart-empty' aria-hidden='true'></span></button>");
                        }
                        echo("</div>");
                        $request_string = "info_restaurant.php?restaurantname=" . $business_name . "&restauranttype=" . $business_category . "&rating=0";
                        echo("<button type='button' class='btn btn-default btn-lg' onClick='cancelButtonUpdate(\"buttons_" . $key . "\",\"" . $button_id . "\",\"" . $request_string . "\")' id='" . $button_id . "'><span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span></button>");
                        echo("</div>");
                        echo("</td>");
                        echo("</div>");
                        echo("</tr>");
                    }
                } else {
                    ?>
                <tr><td style="border-top-width: 0px;"><strong>Error in Yelp API!</strong><br><?php echo($yelp_json['error']['text']) ?></td></tr>
                        <?php
                    }
                ?>
            </table>
        </div>
    </div>
<script src="js/geolocation.js"></script>
<?php include('footer.php'); ?>