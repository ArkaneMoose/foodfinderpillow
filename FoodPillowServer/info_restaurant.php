<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
    die();
}

// Check if restaurant is in restaurants table
$restaurantname = $db->escape_string($_GET['restaurantname']);
$restauranttype = $db->escape_string($_GET['restauranttype']);
$rating = $db->escape_string($_GET['rating']);
$user_id = $db->escape_string($_SESSION['userid']);
$restaurant_query = "SELECT * FROM restaurants WHERE restaurantname='$restaurantname' LIMIT 1";
if (!$restaurant_lookup_result = $db->query($restaurant_query)) {
    die("Unable to look up restaurant because " . $db->error);
}

// If not, add to restaurants table
if ($restaurant_lookup_result->num_rows == 0) {
    $restaurant_query = "INSERT INTO restaurants (restaurantname) VALUES ('$restaurantname')";
    if (!$restaurant_insert_query = $db->query($restaurant_query)) {
        die("Unable to insert restaurant data because " . $db->error);
    }
    $restaurant_id = $db->insert_id;
    $restaurant_types = explode(",", $restauranttype);
    foreach ($restaurant_types as $restaurant_type) {
        $restaurant_query = "INSERT INTO restaurantcategories (restaurantid, restauranttype) VALUES ('$restaurant_id', '$restaurant_type')";
        if (!$restaurant_insert_query = $db->query($restaurant_query)) {
            die("Unable to insert restaurant data because " . $db->error);
        }
    }
} else {
    $restaurant_id = $restaurant_lookup_result->fetch_assoc()['restaurantid'];
}

// Update user preferences
// Check if restaurant is in user prefs table
$prefs_query = "SELECT * FROM userpreferences WHERE userid='$user_id' AND restaurantid='$restaurant_id' LIMIT 1";
if (!$prefs_lookup_result = $db->query($prefs_query)) {
    die("Unable to look up user preferences because " . $db->error);
}
if ($prefs_lookup_result->num_rows == 0) {
    $prefs_query = "INSERT INTO userpreferences (userid, restaurantid, rating) VALUES ('$user_id', '$restaurant_id', '$rating')";
    if (!$prefs_insert_result = $db->query($prefs_query)) {
        die("Unable to insert user preferences because " . $db->error);
    }
} else {
    $pref_id = $prefs_lookup_result->fetch_assoc()['prefid'];
    $prefs_query = "UPDATE userpreferences SET rating=$rating WHERE prefid=$pref_id";
    if (!$prefs_insert_result = $db->query($prefs_query)) {
        die("Unable to update user preferences because " . $db->error);
    }
}

die("OK");