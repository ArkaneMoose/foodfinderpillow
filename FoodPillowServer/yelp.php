<?php
if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || strtolower($_SERVER['HTTP_HOST']) == 'localhost') {
    header('Content-Type: text/plain');
    require_once('../lib/yelp/yelp_api.php');
    echo(yelp_search(isset($_GET['categories']) ? $_GET['categories'] : '', isset($_GET['location']) ? $_GET['location'] : '', null));
}
?>