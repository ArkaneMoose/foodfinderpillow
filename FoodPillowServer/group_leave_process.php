<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
    die();
}
// required params not set redirect to group_join with error parameter invalid params
if (!isset($_POST['groupid'])) {
    header('Location: group_current.php?error=Error:+Invalid+Params');
    die();
}
?>
<?php
$groupid = $db->escape_string($_POST['groupid']);
$userid = $db->escape_string($_SESSION['userid']); // is only set by server anyways
$group_lookup_query = "SELECT status FROM groups WHERE groupid='$groupid'";
if (!$group_lookup_result = $db->query($group_lookup_query)) {
    die("unable to look up groupid because " . $db->error);
}
if ($group_lookup_result->num_rows == 0) {
    // group does not exist redirect to group_join with error parameter group does not exist
    header('Location: group_current.php?error=Error:+Group+does+not+exist');
    die();
}
$group_del_query = "DELETE FROM usergroups WHERE groupid='$groupid' AND userid='$userid';";
if (!$db->query($group_del_query)) {
    die("unable to delete usergroup because " . $db->error);
}
// user group disassociation completed
$group_url = "view_group.php?groupid=" . $groupid;
header('Location: ' . $group_url);
?>