<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
?>
<?php include('header.php'); ?>
<?php
$groupid = $db->escape_string($_GET['groupid']);
$group_lookup_query = "SELECT groupname FROM groups WHERE groupid='$groupid'";
if ($group_lookup_result = $db->query($group_lookup_query)) {
    if ($group_lookup_result->num_rows > 0) {
        $group_entry = $group_lookup_result->fetch_assoc();
        if (isset($group_entry['groupname'])) $groupname = $group_entry['groupname'];
    }
}
?>
<div class="page-header">
        <h3>Rename</h3>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Rename this group</h2>
        </div>
        <div class="panel-body">
            <form action="group_rename_process.php" method="POST">
                <input type="hidden" name="groupid" id="groupid" value="<?php echo(htmlspecialchars($_GET['groupid'])) ?>">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
                    <input type="text" name="groupname" class="form-control" placeholder="Group Name" value="<?php echo(htmlspecialchars($groupname)) ?>" aria-describedby="group-addon">
                </div>
                <br>
                <button role="button" type="submit" class="btn btn-primary">Rename</button> <a role="button" href="view_group.php?groupid=<?php echo(htmlspecialchars($_GET['groupid'])) ?>" class="btn btn-danger">Cancel</a>
            </form>
        </div>
    </div>
<?php include('footer.php'); ?>