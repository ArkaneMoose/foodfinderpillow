<!DOCTYPE html>
<html>
    <head>
        <title>EatOwl</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">EatOwl</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="group_create.php">Create group</a></li>
                        <li><a href="group_join.php">Join group</a></li>
                        <li><a href="group_current.php">Your groups</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                        if (isset($_SESSION['userid'])) {
                        ?>
                        <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                        <?php
                        } else {
                        ?>
                        <li><a href="register_user.php"><span class="glyphicon glyphicon-user"></span> Register</a></li>
                        <li><a href="login_user.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                        <?php
                        }
                        ?></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">