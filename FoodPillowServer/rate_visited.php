<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
?>
<?php include('header.php'); ?>
<div class="page-header">
    <h3>Rate Restaurants You've Visited</h3>
</div>
    <?php
    $userid = $db->escape_string($_SESSION['userid']);
    $restaurant_lookup_query = "SELECT restaurants.restaurantid FROM usergroups INNER JOIN groups ON usergroups.groupid=groups.groupid INNER JOIN restaurants ON groups.restaurantid=restaurants.restaurantid WHERE usergroups.userid='$userid'";
    if ($restaurant_lookup_result = $db->query($restaurant_lookup_query)) {
        if ($restaurant_lookup_result->num_rows > 0) {
            while ($restaurant_entry = $restaurant_lookup_result->fetch_assoc()) {
                
            }
        }
    } else {
        ?>
        <p>Failed to connect to database. Try <a href="group_current.php">reloading</a> the page.</p>
        <p><strong>Error details:</strong><br><?php
            echo(htmlspecialchars($db->error));
        ?></p>
        <?php
    }
    ?>
<?php include('footer.php'); ?>