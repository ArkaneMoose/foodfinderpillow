<?php 
session_start();
include('config.php'); 
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
?>
<?php include('header.php'); ?>
<div class="page-header"><h3>Your Groups</h3></div>
<div class="btn-group-vertical btn-block">
    <?php
    $userid = $db->escape_string($_SESSION['userid']);
    $query = "SELECT usergroups2.groupid, groups.groupname, users.userid, users.username FROM usergroups INNER JOIN usergroups AS usergroups2 ON usergroups.groupid=usergroups2.groupid INNER JOIN groups ON usergroups2.groupid=groups.groupid INNER JOIN users ON usergroups2.userid=users.userid WHERE usergroups.userid=$userid;";
    if (!$query_result = $db->query($query)) {
        ?>
        <p>Failed to connect to database. Try <a href="group_current.php">reloading</a> the page.</p>
        <p><strong>Error details:</strong><br><?php
            echo(htmlspecialchars($db->error));
        ?></p>
        <?php
    }
    if ($query_result->num_rows == 0) {
        ?>
        <p>You don't have any groups. :(</p>
        <p><a href="group_create.php">Create one now!</a></p>
        <?php
    }
    $all_groups = array();
    while ($group_row = $query_result->fetch_assoc()) {
        if (!isset($all_groups[$group_row['groupid']])) {
            $all_groups[$group_row['groupid']] = array($group_row['groupname']);
        }
        if ($group_row['userid'] == $_SESSION['userid']) {
            $all_groups[$group_row['groupid']][count($all_groups[$group_row['groupid']])] = 'You';
        } else {
            $all_groups[$group_row['groupid']][count($all_groups[$group_row['groupid']])] = $group_row['username'];
        }
    }
        foreach ($all_groups as $groupid => $group) {
            ?>
            <a href="view_group.php?groupid=<?php echo($groupid); ?>" class="btn btn-default btn-block btn-lg" style="text-align: left;" role="button">
                <strong><?php
                echo(htmlspecialchars($group[0]))
                ?></strong><br>
            <?php
            foreach (array_slice($group, 1) as $key => $user_in_group) {
                if ($key > 0) {
                    echo(", ");
                }
                echo(htmlspecialchars($user_in_group));
            }
            ?>
            </a>
            <?php
        }
    ?>
</div>
<?php include('footer.php'); ?>