<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_POST['username']) || !isset($_POST['password'])) {
    header('Location: index.php');
}
?>
<?php
$username = $db->escape_string($_POST['username']);
$password = $db->escape_string($_POST['password']);
$password_hash = password_hash($password, PASSWORD_DEFAULT);
$create_user_query = "INSERT INTO users (username, password) VALUES ('$username', '$password_hash')";
if (!$db->query($create_user_query)) {
    die("unable to create user because " . $db->error);
}
$user_id = $db->insert_id;
$_SESSION['userid'] = $user_id;
// user is now logged in
header('Location: index.php');
?>