<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
    die();
}
// required params not set redirect to group_join with error parameter invalid params
if ($_POST['groupname'] == '') {
    header('Location: group_create.php?error=Error:+Invalid+Params');
    die();
}

$groupname = $db->escape_string($_POST['groupname']);
$userid = $db->escape_string($_SESSION['userid']); // is only set by server anyways
$group_create_query = "INSERT INTO groups (groupname, status) VALUES ('$groupname', '0')";
if (!$db->query($group_create_query)) {
    die("unable to create group because " . $db->error);
}
$groupid = $db->insert_id;
$group_join_query = "INSERT INTO usergroups (groupid, userid) VALUES ('$groupid', '$userid')";
if (!$db->query($group_join_query)) {
    die("unable to join group because " . $db->error);
}
$group_url = "view_group.php?groupid=" . $groupid; 
header('Location: ' . $group_url);
?>