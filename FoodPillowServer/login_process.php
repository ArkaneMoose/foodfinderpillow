<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_POST['username']) || !isset($_POST['password'])) {
    header('Location: login_user.php?error=Both+the+username+and+password+fields+are+required.');
    die();
}
?>
<?php
$username = $db->escape_string($_POST['username']);
$password = $db->escape_string($_POST['password']);
$login_query = "SELECT userid, password FROM users WHERE username='$username'";
if (!$login_query_result = $db->query($login_query)) {
    die("unable to find user because " . $db->error);
}
if ($login_query_result->num_rows == 0) {
    // user does not exist
    header('Location: login_user.php?error=Incorrect+username+and%2For+password.');
    die();
}
$user_row = $login_query_result->fetch_assoc();
$user_password = $user_row['password'];
$user_id = $user_row['userid'];
if (!password_verify($password, $user_password)) {
    // password incorrect
    header('Location: login_user.php?error=Incorrect+username+and%2For+password.');
    die();
}
$_SESSION['userid'] = $user_id;
// user is now logged in
header('Location: index.php');
?>