<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
}
?>
<?php include('header.php'); ?>
<div class="page-header">
        <h3>Create</h3>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Create a Group</h2>
        </div>
        <div class="panel-body">
            <form action="group_create_process.php" method="post">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="group-addon">+</span>
                    <input type="text" name="groupname" class="form-control" placeholder="Group Name" aria-describedby="group-addon">
                </div>
                <br>
                <input type="submit" value="Create" class="btn btn-primary">
            </form>
        </div>
    </div>
<?php include('footer.php'); ?>