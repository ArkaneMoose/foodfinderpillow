CREATE TABLE users(userid int(11) NOT NULL AUTO_INCREMENT UNIQUE, username varchar(20) NOT NULL UNIQUE, password varchar(255) NOT NULL, PRIMARY KEY(userid));
CREATE TABLE groups(groupid int(11) NOT NULL AUTO_INCREMENT UNIQUE, groupname varchar(20) NOT NULL, restaurantid int(11), status tinyint NOT NULL, PRIMARY KEY(groupid));
CREATE TABLE usergroups(groupid int(11) NOT NULL, userid int(11) NOT NULL, PRIMARY KEY(groupid, userid));
CREATE TABLE userpreferences(prefid int(11) NOT NULL AUTO_INCREMENT UNIQUE, userid int(11) NOT NULL, restaurantid int(11) NOT NULL, rating tinyint NOT NULL, PRIMARY KEY(prefid));
CREATE TABLE restaurants(restaurantid int(11) NOT NULL AUTO_INCREMENT UNIQUE, restaurantname varchar(100) NOT NULL, PRIMARY KEY(restaurantid));
CREATE TABLE restaurantcategories(restaurantid int(11) NOT NULL, restauranttype varchar(20) NOT NULL);