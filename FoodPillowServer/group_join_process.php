<?php
session_start();
include('config.php');
?>
<?php
if (!isset($_SESSION['userid'])) {
    header('Location: index.php');
    die();
}
// required params not set redirect to group_join with error parameter invalid params
if (!isset($_POST['groupid'])) {
    header('Location: group_join.php?error=Error:+Invalid+Params');
    die();
}
?>
<?php
$groupid = $db->escape_string($_POST['groupid']);
$userid = $db->escape_string($_SESSION['userid']); // is only set by server anyways
$group_lookup_query = "SELECT status FROM groups WHERE groupid='$groupid'";
if (!$group_lookup_result = $db->query($group_lookup_query)) {
    die("unable to look up groupid because " . $db->error);
}
if ($group_lookup_result->num_rows == 0) {
    // group does not exist redirect to group_join with error parameter group does not exist
    header('Location: group_join.php?error=Error:+Group+does+not+exist');
    die();
}
$group_lookup_query = "SELECT * FROM usergroups WHERE groupid='$groupid' AND userid='$userid'";
if (!$group_lookup_result = $db->query($group_lookup_query)) {
    die("unable to look up the status of your presence in this group because " . $db->error);
}
if ($group_lookup_result->num_rows > 0) {
    // group does not exist redirect to group_join with error parameter group does not exist
    header('Location: group_join.php?error=Error:+You+are+already+in+this+group');
    die();
}
$group_add_query = "INSERT INTO usergroups (groupid, userid) VALUES ('$groupid', '$userid')";
if (!$db->query($group_add_query)) {
    die("unable to insert usergroup because " . $db->error);
}
// user group association completed
$group_url = "view_group.php?groupid=" . $groupid;
header('Location: ' . $group_url);
?>